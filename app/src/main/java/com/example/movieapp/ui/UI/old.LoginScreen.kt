package com.example.movieapp.ui.UI


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.movieapp.R
import com.example.movieapp.ui.UI.Theme.MovieAppTheme
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MovieAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    LogInScreenOld()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LogInScreenOld(modifier: Modifier = Modifier) {
    var nameText by remember { mutableStateOf("") }
    var passwordText by remember { mutableStateOf("") }
    var loggedIntoApp by remember { mutableStateOf(false) }
    var loginFailed by remember { mutableStateOf(false) }
    Column {
        Column(horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .weight(.5f)
                .background(MaterialTheme.colorScheme.background))
        {
            Text(stringResource(R.string.app_name),
                style = MaterialTheme.typography.displayLarge,
                color = MaterialTheme.colorScheme.primary
            )
        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .weight(.7f)
                .background(MaterialTheme.colorScheme.background)
        )
        {
            Image(
                painter = painterResource(id = R.drawable.baseline_live_tv_24),
                contentDescription = "Tv Icon",
                modifier = Modifier.size(75.dp),
                colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.primary)
            )
        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .background(MaterialTheme.colorScheme.background)
        )
        {
            TextField(
                value = nameText,
                onValueChange = { nameText = it },
                label = { Text("Username") },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                isError = loginFailed,
                colors = TextFieldDefaults.textFieldColors(containerColor = MaterialTheme.colorScheme.primaryContainer),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Divider(
                modifier = Modifier.height(1.dp),
                color = MaterialTheme.colorScheme.background
            )
            TextField(
                value = passwordText,
                onValueChange = { passwordText = it },
                label = { Text("Password") },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                isError = loginFailed,
                colors = TextFieldDefaults.textFieldColors(containerColor = MaterialTheme.colorScheme.primaryContainer),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            if (!loginFailed) {
                Divider(
                    modifier = Modifier.height(30.dp),
                    color = MaterialTheme.colorScheme.background
                )
            }
            else {
                Text(
                    text = "Incorrect Username or Password",
                    style = MaterialTheme.typography.bodyLarge,
                    color = MaterialTheme.colorScheme.error,
                )
                Divider(
                    modifier = Modifier.height(9.dp),
                    color = MaterialTheme.colorScheme.background
                )
            }
            Button(onClick = {
                if (nameText == "Movie" && passwordText == "Watcher") {
                    loggedIntoApp = true
                    loginFailed = false
                }
                else {
                    loginFailed = true
                }
            }) {
                Text(text = "Login")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LogInPreviewOld() {
    MovieAppTheme (darkTheme = false){

        LogInScreenOld()
    }
}
@Preview(showBackground = true)
@Composable
fun LogInPreviewDarkOld() {
    MovieAppTheme (darkTheme = true){

        LogInScreenOld()
    }
}