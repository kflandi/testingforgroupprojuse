package com.example.movieapp.watchlist

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Name(
    val title: String,
    //val phoneNumber: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0

)
