package com.example.movieapp.watchlist

data class ContactState(
    val names: List<Name> = emptyList(),
    val title: String="" ,
   // val phoneNumber: String ="" ,
    val isAdding: Boolean = false,
    val sortType: SortType = SortType.TITLE
)
