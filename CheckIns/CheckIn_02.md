Caleb Elkevizth:

**What have you contributed to the project between the last check in and this check in?**
    I have built and added a login screen to the project.  It has functioning textfields although actually making it fully operational
will take quite a bit more work as I am not yet sure how we are even going to maintain user login information.  I also added preview 
functions to the Main Activity that show the different screens for our app next to each other.
**What have you learned between the last check in and this check in?**
    Since last check in we learned about creating themes for our apps which can help standardize colors, shapes, and more throughout
an application.
**What do you need help with?**
    We have quite a bit ahead of us still but at the moment what we need help with most is switching between activities.  We have
two screen UIs built and a third one on the way but we currently have no way of making them work together as one app.
**What do you expect to complete between the next check in and this check in?**
    I believe that we will have connected our various activities into one app that can flip between activities.  We already have 
the activities built, they just need to be put together.


Isaiah Ramirez:  
**What have you contributed to the project between the last check in and this check in?**  
	- I have remade the entire homepage with the new skills that we have learned in class, I started importing our own themed with our own colors, I also took all the code for the homepage and moved it into it's own file from the mainactivity to clean up the project and keep everything organized.  

**What have you learned between the last check in and this check in?**  
	- I've learned better ways to build the homepage, learned how to implement custom colors for the theme, and the good practices to have when working on a project such as keeping the code out of the main activity and separating the code into its own file.

**What do you need help with?**  
	- I think as of right now there's not much that we really need help with, but I'm sure as we work on it more and try to implement more stuff down the road we will need some help.

**What do you expect to complete between the next check in and this check in?**  
	- I'm hoping that by our next check in we can have some navigation implemented so that we can move between the different screens in our app.  

Kayden Landis:

**What have you contributed to the project between the last check in and this check in?**

-Since the last check in, I have come close to completing a list where user input of movies and tv shows can be stored into a database. I am working in a different android studio file than our main repository file just to be extra cautious to not screw up anything within it when I experiment with creating the list. This is to ensure when I do try and implement it into our project I don’t accidentally add any unwanted changes along the way that could interfere with the program or cause errors. Overall, I have created the structure of what I want it to look like (a scrollable column containing cards), I just need to figure out how to store user input properly into a database.

**What have you learned between the last check in and this check in?**

-Since the last check in, I have learned a pretty broad range of things including how to add custom fonts in a separate file, how to take user input, and almost learned how to implement a database into our app. With custom things like fonts, you can add the properties you want it to follow into a separate font file. I plan on using other files similar to this to have the general structure of things (like our list) to make our app's code easier to understand and make changes to. User input is not as difficult as I thought it would be to record. While browsing online I discovered that TextField can easily be used to record whatever a user types.
    
**What do you need help with?**

-This coming week I may ask for a little help completing my list database combination. I think I am just around the corner from completing it, I just keep running into errors when I try to implement a database in for user input. Hopefully by the end of this weekend I can figure out what I am doing wrong and continue from there. 

**What do you expect to complete between the next check in and this check in?**

-I am planning to figure out and complete the list with its corresponding database as well as add it to our project. If I have the time, I will start looking into how we can include multiple pages for our app and begin to setup the page for the user to add the tv shows/movies that they would like to.
 
