**Caleb Elkevizth:**
**What have you contributed to the project between the last check in and this check in?**
I have updated the app icon and included it on the login screen.

**What have you learned between the last check in and this check in?**
We went over using SQL and I also worked on understanding updating the error state for the login screen for our project

**What do you need help with?**
I believe that our app is quite far along, and as far as what I have been working on, I figured it out Friday in class.

**What do you expect to complete between the next check in and this check in?**
I believe that we should have our final screen completed, Kayden has gotten pretty far with it I believe.


**Isaiah Ramirez:**  
**What have you contributed to the project between the last check in and this check in?**  
-I think our app is very close to being finished so there isn't much left to work on, but I worked on the code that will randomly select a movie from the user's watchlist and display it one the screen.

**What have you learned between the last check in and this check in?**  
-While we have learned things in class like sql, in regards to our actual app I don't think I've learned too much since our last check in. The reason being that we are so close to being done that we got most of the harder stuff completed already so now it's just finishing up some of the smaller things left for the most part.

**What do you need help with?**  
-I think our app is in a really good place right now, I think we are pretty close to having everything completed for our app. As of right now I don't think there is really anything that we need help with.  

**What do you expect to complete between the next check in and this check in?**  
- I'm hoping that by the next check in we can have the watchlist implemented and have the last function we need to randomly select a item from the watchlist and display it to the user when the button is clicked.  

Kayden Landis:
**What have you contributed to the project between the last check in and this check in?**

Between this check in and the last, I have finally finished building the third page of our app. This page takes user input of what movie or show they want to add, adds that information to a database, and allows the user to delete titles.

**What have you learned between the last check in and this check in?**

I have learned how to correctly implement a scrollable list of user input that has a delete function added to it. Other than this, I got a better understanding of how to store user entered data and how to effectively swap and display different screens within an app.

**What do you need help with?**

I may need help next week with verifying I have set everything up corectly. I am just making sure I am running things optimally and that there are no potential leaks. 

**What do you expect to complete between the next check in and this check in?**

I expect to edit the third page of our app to organize it better and potentially add a few more features to it like sorting or a list search.











