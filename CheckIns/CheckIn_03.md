Isaiah Ramirez:  
**What have you contributed to the project between the last check in and this check in?**  
	- I've been working on trying to implement navigation in our app, but unfortunately I ran into some errors while trying to get it to work and as of right now I broke our app so I haven't pushed any changes because I'm trying to fix our app.

**What have you learned between the last check in and this check in?**  
	- We have learned the two different ways to implement navigation, so if I can't get it to work maybe I'll try the other form of navigation to see if I have better luck with that one instead.

**What do you need help with?**  
	- I'm hoping to have the navigation problems figured out but if I don't then I will probably need help trying to implement it, but I'm hoping we can find a solution on our own.

**What do you expect to complete between the next check in and this check in?**  
	- I'm hoping that by our next check in we can have the navigation fixed and working properly.


	Kayden Landis:  
**What have you contributed to the project between the last check in and this check in?**  
	- I have finally found and followed a good tutorial on how to add in a screen to add movie/show titles to our application. It has a scrollable list and database to hold and show all of the information entered by users. I plan on adding it to the project soon after I remove a couple of features from it that I don't think we will need. 

**What have you learned between the last check in and this check in?**  
	- Since the last check in, I have learned how to implement a database and how to record user input. I have also learned how to use information from a database and add it to UI elements in our app.

**What do you need help with?**  
	- I think as of now I am pretty set with adding the add page to our app. 

**What do you expect to complete between the next check in and this check in?**  
	- I am hoping by our next check in that I am able to add in the user input page to record user input.